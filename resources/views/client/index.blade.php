@extends ('layouts.master')

@section ('content')
	<div id="content-wrapper">

	    <div class="container-fluid">
	    	<h1>Clients</h1>
	      <table class="table">
		  <thead class="thead-dark">
		    <tr>
		      <th scope="col">id</th>
		      <th scope="col">First Name</th>
		      <th scope="col">Middle Name</th>
		      <th scope="col">Last Name</th>
		      <th scope="col">Address</th>
		      <th scope="col">Email</th>
		      <th scope="col">Contact Number</th>
		      <th scope="col">Actions</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach ($clients as $client)
		  		
			    <tr>
			      <th scope="row">{{ $client->id }}</th>
			      <td>{{ $client->fname }}</td>
			      <td>{{ $client->mname }}</td>
			      <td>{{ $client->lname }}</td>
			      <td>{{ $client->exact_addr }}</td>
			      <td>{{ $client->e_mail }}</td>
			      <td>{{ $client->phone_num }}</td>
			      <td><a class="btn btn-primary" href="/client/{{ $client->id }}">VIEW</a></td>
			    </tr>
				
		    @endforeach
		  </tbody>
		  </table>
	    </div>
    <!-- /.container-fluid -->

  	</div>
  	<!-- /.content-wrapper -->
@endsection