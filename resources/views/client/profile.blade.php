@extends ('layouts.master')

@section ('content')

	<div class="container-wrapper col-lg-6 offset-lg-2">
		<div class="container">
			<h1>Client Profile</h1>
			<hr>
			<form>
			  <div class="form-group">
			    <label for="fname">First Name</label>
			    <input type="text" readonly class="form-control" id="fname" aria-describedby="fname" value="{{ $client->fname }}">
			  </div>
			  <div class="form-group">
			    <label for="mname">Middle Name</label>
			    <input type="text" readonly class="form-control" id="mname" aria-describedby="mname" value="{{ $client->mname }}">
			  </div>
			  <div class="form-group">
			    <label for="lname">Last Name</label>
			    <input type="text" readonly class="form-control" id="lname" aria-describedby="lname" value="{{ $client->lname }}">
			  </div>
			  <div class="form-group">
			    <label for="Address">Address</label>
			    <input type="text" readonly class="form-control" id="Address" aria-describedby="Address" value="{{ $client->exact_addr }}">
			  </div>
			  <div class="form-group">
			    <label for="Email">Email</label>
			    <input type="text" readonly class="form-control" id="Email" aria-describedby="Email" value="{{ $client->e_mail }}">
			  </div>
			  <div class="form-group">
			    <label for="phone_num">Contact Number</label>
			    <input type="text" readonly class="form-control" id="phone_num" aria-describedby="phone_num" value="{{ $client->phone_num }}">
			  </div>
			</form>
		</div>
	</div>

@endsection