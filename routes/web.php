<?php


Route::get('/', function () {
    return view('welcome');
});


Route::get('client', 'ClientController@clientList');

Route::get('client/{id}', 'ClientController@client');