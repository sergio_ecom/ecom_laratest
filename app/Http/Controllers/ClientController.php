<?php

namespace App\Http\Controllers;

use App\Client;

class ClientController extends Controller
{
    public function clientList()
    {
    	$clients = Client::all();

        return view('client.index', compact('clients'));
    }

    public function client($id)
    {

    	$client = Client::find($id);

    	return view('client.profile', compact('client'));
    }
}
